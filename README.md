# MatLib

by **Uphill=>Upskill**

## LIVE PROJECT

[link to GitLab Pages](https://kristiqntashev.gitlab.io/matlib/)

## MOCKUP

[link to Zeplin](https://scene.zeplin.io/project/5fbd730009cdd092cc944a51)

## USER JOURNEY

**Tim is 3d designer looking for nice materials for his latest project.** Tim is provided with an account to the MatCat and a link to the application. 

### Sign in page

He follows the link and lands on a sign in screen, requesting a username and password. Tim enters his credentials and clicks the Sign In button. 

### Grid view

Now he sees the MatLib grid. Tim can view 3 rendered images on a row, part of infinite scroll grid. Each material tile has image in the center, Id, title and a filename at the left bottom corner. There is indication for favoring in the top right corner as well. Tim can scroll trough the images. 

### Filter

Tim would like to find faster the best matching materials, so he needs to filter the material boxes. He can filter by material type, color and tags. Type and tags are presented as checkboxes with text labels, colors are circular checkboxes in the corresponding color with text labels beneath them.

- Tim can select the criteria he needs by checking and unchecking the checkboxes for each filter item. The filtering is performed on click. 

- He can remove all filters at once by clicking the Clear All button at the top of the Filter panel, that becomes visible once at least one filtering criteria is selected. 

- Tim can review his selection also by checking the pill elements on the top of the grid and remove any of them that are no longer needed.

### Sort

Tim wants to be able to sort the result of the filtering by name or ID, so he can use the Sort By dropdown at the top right of the grid view. Right in front of it he can see how many results are displayed in the grid.

### Search

He needs also a fast way to find a specific material. To achieve that, he can use the search field next to Sort By dropdown.

### Favorites

Once Tim finds materials he likes, he wants to be able to save them as favorites in his profile. He can do so by clicking the favorite icon in the top right corner of the material tile he likes. 

He also wants to see his favorites  - he can do so by clicking the favorites icon in the top bar of the page. Tim can see how many items has he favored by checking the small number next to the icon. Once clicked on the icon, Tim sees only the favorited materials. He can filter and sort them the same manner he does with the full material list. He also can unfavor/unsave from his profile any material, by unclicking the favorite icon at the right top corner of the material tile.

Mat can go back to the full catalog by clicking the logo in the top bar.

### Profile

Tim wants to be able to make changes to his profile. He can access this information by clicking on his name/avatar in the top bar. That action brings him to Profile page, where he can sign out of his account by clicking the link next to the greeting message, or edit his details by clicking the edit button. By doing so he can edit first, last name and upload new profile picture. He can either cancel the changes or save them by clicking the Cancel or Save Changes buttons.

Mat can go back to the full catalog by clicking the logo in the top bar.

=====================================

*Thanks for reading*


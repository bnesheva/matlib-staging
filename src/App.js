import React, { useState } from "react";
import { hot } from "react-hot-loader/root";
import "./App.css";
import { HashRouter as Router } from "react-router-dom";
import BackToTop from "./components/grid/backtotop/BackToTop";
import Header from "./containers/header/Header";
import MainContainer from "./containers/mainContainer/mainContainer";

export const App = () => {
  const [headerPin, setHeaderPin] = useState(false);

  function togglePin() {
    setHeaderPin(!headerPin);
  }
  return (
    <Router>
      <div className={headerPin ? "App pinned" : "App"}>
        <Header togglePin={togglePin} headerPin={headerPin} />
        <MainContainer />
        <BackToTop />
      </div>
    </Router>
  );
};

export default hot(App);

import React from "react";
import { shallow } from "enzyme";
import App from "./App.js";

describe("Test component: <App />", () => {
  it("Renders without crashing: <App />", () => {
    const wrapper = shallow(<App />);
    expect(wrapper.exists()).toBe(true);
  });
});

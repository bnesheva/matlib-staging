import { LOG_IN } from "./constants";
import { LOG_OUT } from "./constants";

export const logIn = (user) => ({
  type: LOG_IN,
  user
});
export const logOut = (user) => ({
  type: LOG_OUT,
  user
});

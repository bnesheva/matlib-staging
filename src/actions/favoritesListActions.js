import { ADD_FAV, REMOVE_FAV, FETCH_FAVORITES_SUCCESS } from "./constants";

export const addFav = (productId) => ({
  type: ADD_FAV,
  productId
});

export const removeFav = (productId) => ({
  type: REMOVE_FAV,
  productId
});

export const getFavs = (favorites) => ({
  type: FETCH_FAVORITES_SUCCESS,
  favorites
});

import {
  SPINNER,
  FETCH_SUCCESS,
  FETCH_SUCCESS_MANUFACTURER,
  CLEAR_PRODUCTS
} from "./constants";

export const fetchProductsSpinner = () => ({
  type: SPINNER
});

export const fetchProductsSuccess = (products) => ({
  type: FETCH_SUCCESS,
  products,
  productNum: products.length
});

export const clearProducts = (products) => ({
  type: CLEAR_PRODUCTS,
  products
});

export const fetchProductManufacturersAction = (manufacturers) => ({
  type: FETCH_SUCCESS_MANUFACTURER,
  manufacturers
});

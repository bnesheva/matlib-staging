import { SEARCH } from "./constants";

export const search = (searching) => ({
  type: SEARCH,
  searching
});

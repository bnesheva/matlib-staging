import { SORT } from "./constants";

export const sort = (sorterType) => ({
  type: SORT,
  sorterType
});

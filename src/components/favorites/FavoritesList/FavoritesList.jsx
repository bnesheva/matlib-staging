import React, { useEffect } from "react";
import Filter from "../../../containers/filter/Filter";
import PropTypes from "prop-types";
import fetchInitialFilters from "../../../services/filter/fetchInitialFilters";
import NoResult from "../../../components/grid/noresult/NoResult";

export const FavoritesList = (props) => {
  const { favList, fetchFavoritesList } = props;
  return favList.length > 0 ? (
    getList(fetchFavoritesList, favList)
  ) : (
    <NoResult message="There are no tiles in your favorites list" />
  );
};

const getUrlOfUserFavorites = (favorites) => {
  const result = favorites.map((favorite) => favorite.vrscanId);
  return "&id=".concat(result.join("&id="));
};

FavoritesList.propTypes = {
  favList: PropTypes.array,
  fetchFavoritesList: PropTypes.func
};

const getList = (fetchFavoritesList, favList) => {
  useEffect(() => {
    fetchFavoritesList();
  }, []);
  const favUrl = getUrlOfUserFavorites(favList);
  return <Filter productIds={favUrl} filterNames={fetchInitialFilters()} />;
};

import React from "react";
import { shallow } from "enzyme";
import renderer from "react-test-renderer";

import { FavoritesList } from "./FavoritesList";
import { fetchFavorites } from "../../../services/useFetchProducts";

const mockProps = {
  favList: [{ id: 1 }, { id: 2 }, { id: 3 }],
  fetchFavoritesList: fetchFavorites
};

let component;

describe("Counter Component", () => {
  it("returns Filter", () => {
    component = shallow(<FavoritesList {...mockProps} />);
    // component = renderer.create(<FavoritesList {...mockProps} />).toJSON();
    expect(component).toMatchSnapshot();
  });
});

import React from "react";
import PropTypes from "prop-types";

export const FavoritesNumber = (props) => {
  const { favList } = props;
  const numberOfFavs = favList.length;
  return <>{numberOfFavs}</>;
};

FavoritesNumber.propTypes = {
  favList: PropTypes.array
};

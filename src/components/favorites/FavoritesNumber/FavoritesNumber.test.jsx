import React from "react";
import renderer from "react-test-renderer";
import { FavoritesNumber } from "./FavoritesNumber";

it("renders favorites number correctly", () => {
  const arrOfFavs = [{ fav: 1 }];
  const tree = renderer.create(<FavoritesNumber favList={arrOfFavs} />).toJSON();
  expect(tree).toMatchSnapshot();
});

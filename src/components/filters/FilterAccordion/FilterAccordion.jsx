import React, { useState } from "react";
import propTypes from "prop-types";
import Options from "../Options/Options";
import "./filter-accordion.css";
function FilterAccordion(props) {
  const [expanded, setExpanded] = useState(true);
  function toggleAccordion() {
    setExpanded(!expanded);
  }

  return (
    <>
      <div
        className={
          expanded ? "filter-accordion" : "filter-accordion filter-accordion--folded"
        }
        key={props.filterType}
      >
        <div className="filter-accordion--header" onClick={toggleAccordion}>
          <h4>{props.filterType}</h4>
          <span className="filter-accordion--header--chevron">
            <i className="fas fa-chevron-down"></i>
          </span>
        </div>
        <div className="filter-accordion--card">
          <ul>
            <Options {...props} />
          </ul>
        </div>
      </div>
    </>
  );
}

FilterAccordion.propTypes = {
  filterType: propTypes.string.isRequired,
  handleChange: propTypes.func.isRequired,
  currentFilters: propTypes.array.isRequired
};

export default FilterAccordion;

import React from "react";
import { shallow } from "enzyme";
import FilterAccordion from "./FilterAccordion";
import { default as data } from "../../../../data";

describe("Test component: <FilterAccordion />", () => {
  it("Renders tags without crashing: <FilterAccordion />", () => {
    const filterAccordion = shallow(
      <FilterAccordion
        data={data}
        currentFilters={[]}
        handleChange={() => {}}
        filterType={"materials"}
      />
    );

    expect(filterAccordion.exists()).toBe(true);
  });

  it("Should render 3 divs in FilterAccordion", () => {
    const wrapper = shallow(
      <FilterAccordion
        data={data}
        currentFilters={[]}
        handleChange={() => {}}
        filterType={"materials"}
      />
    );
    expect(wrapper.find("div").length).toBe(3);
  });

  it("Should render accordion--folded class in FilterAccordion", () => {
    const wrapper = shallow(
      <FilterAccordion
        data={data}
        currentFilters={[]}
        handleChange={() => {}}
        filterType={"materials"}
      />
    );
    wrapper.find(".filter-accordion--header").simulate("click");
    expect(wrapper.find(".filter-accordion--folded").exists()).toBe(true);
  });

  it("Should render filter-accordion class in FilterAccordion", () => {
    const wrapper = shallow(
      <FilterAccordion
        data={data}
        currentFilters={[]}
        handleChange={() => {}}
        filterType={"materials"}
      />
    );
    wrapper.find(".filter-accordion--header").simulate("click");
    wrapper.find(".filter-accordion--header").simulate("click");
    expect(wrapper.find(".filter-accordion").exists()).toBe(true);
  });
});

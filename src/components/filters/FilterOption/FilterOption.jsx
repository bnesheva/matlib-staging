import propTypes from "prop-types";
import React from "react";
import "./filter-option.css";

function FilterOption(props) {
  const shining = [22, 9, 5];
  const isChecked = props.currentFilters.includes(
    props.filterType + "_" + props.option.id + "_" + props.option.name
  );
  function makeGradient(color) {
    if (shining.includes(props.option.id)) {
      return {
        backgroundColor: color,
        background: `linear-gradient(45deg,  ${color} 30%,rgba(255,255,255,1) 50%,${color} 80%)`
      };
    } else {
      return { background: color };
    }
  }
  return (
    <>
      <li
        className={
          props.isColor
            ? "filter-option filter-option--color-swatch"
            : "filter-option filter-option--text"
        }
      >
        <input
          type="checkbox"
          id={props.filterType + props.option.id}
          name={props.filterType + "_" + props.option.id + "_" + props.option.name}
          checked={isChecked ? "checked" : ""}
          onChange={props.handleChange}
        />
        <label htmlFor={props.filterType + props.option.id}>
          <div
            className="filter-option--custom-check"
            style={props.isColor ? makeGradient(props.option.hex) : {}}
          >
            <i className="fas fa-check"></i>
          </div>
          {props.option.name}
        </label>
      </li>
    </>
  );
}

FilterOption.propTypes = {
  filterType: propTypes.string.isRequired,
  handleChange: propTypes.func.isRequired,
  isColor: propTypes.bool.isRequired,
  option: propTypes.object.isRequired,
  currentFilters: propTypes.array.isRequired
};

export default FilterOption;

import React from "react";
import propTypes from "prop-types";
import "./filter-pill.css";

function FilterPill(props) {
  const key = props.filter.split("_")[0];
  const isTag = key === "tags";
  return (
    <>
      <div className="pill" key={props.filter}>
        <input
          type="radio"
          id={props.filter}
          name={props.filter}
          onClick={props.handleChange}
        />
        <label htmlFor={props.filter}>
          {isTag ? key.slice(0, -1) + ": " : ""}
          {props.filter.split("_")[2]} &nbsp;<i className="fas fa-times"></i>
        </label>
      </div>
    </>
  );
}

FilterPill.propTypes = {
  filter: propTypes.string.isRequired,
  handleChange: propTypes.func.isRequired
};

export default FilterPill;

import React, { useState } from "react";
import propTypes from "prop-types";
import "./filters.css";
import FilterAccordion from "../FilterAccordion/FilterAccordion";
import withFilterType from "../../../services/filter/withFilterType";

function Filters(props) {
  const [mobileExpanded, setMobileExpanded] = useState(false);
  function toggleFilter() {
    setMobileExpanded(!mobileExpanded);
  }
  function renderFilters() {
    const filterTypes = ["materials", "colors", "tags"];
    return filterTypes.map((filter) =>
      withFilterType(FilterAccordion, filter, props)
    );
  }
  return (
    <div className={mobileExpanded ? "filters" : "filters--folded"}>
      <div className="filters--header">
        <span className="filters--header--label">Filter by:</span>
        <div className="filters--header--label--mobile" onClick={toggleFilter}>
          Filter by&nbsp;
          <span className="filters--header--label--mobile--chevron">
            <i className="fas fa-chevron-down"></i>
          </span>
        </div>
        <div
          className={
            props.currentFilters.length ? "filters--header--clear" : "hidden"
          }
          onClick={props.handleChange}
        >
          Clear all <i className="fas fa-times"></i>
        </div>
      </div>
      <div className="filters--container">{renderFilters()}</div>
    </div>
  );
}

Filters.propTypes = {
  handleChange: propTypes.func.isRequired,
  currentFilters: propTypes.array.isRequired
};

export default Filters;

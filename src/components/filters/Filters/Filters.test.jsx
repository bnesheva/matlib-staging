import React from "react";
import { shallow } from "enzyme";
import Filters from "./Filters";
import { default as data } from "../../../../data";

describe("Test component: <Filters />", () => {
  it("Renders tags without crashing: <Filters />", () => {
    const wrapper = shallow(
      <Filters data={data} currentFilters={[]} handleChange={() => {}} />
    );

    expect(wrapper.exists()).toBe(true);
  });

  it("Should render 5 divs in Filters", () => {
    const wrapper = shallow(
      <Filters data={data} currentFilters={[]} handleChange={() => {}} />
    );
    expect(wrapper.find("div").length).toBe(5);
  });

  it("Should render filters class in Filters", () => {
    const wrapper = shallow(
      <Filters data={data} currentFilters={[]} handleChange={() => {}} />
    );
    wrapper.find(".filters--header--label--mobile").simulate("click");

    const filtersContainer = wrapper.find(".filters");
    expect(filtersContainer.exists()).toBe(true);
  });

  it("Should render filters--folded class in Filters", () => {
    const wrapper = shallow(
      <Filters data={data} currentFilters={[]} handleChange={() => {}} />
    );
    wrapper.find(".filters--header--label--mobile").simulate("click");
    wrapper.find(".filters--header--label--mobile").simulate("click");

    const filtersContainerFolded = wrapper.find(".filters--folded");
    expect(filtersContainerFolded.exists()).toBe(true);
  });

  it("Should render ClearAll button in Filters", () => {
    const wrapper = shallow(
      <Filters
        data={data}
        currentFilters={["materials_2_Leather"]}
        handleChange={() => {}}
      />
    );

    const clearAllButton = wrapper.find(".filters--header--clear");

    expect(clearAllButton.exists()).toBe(true);
  });
});

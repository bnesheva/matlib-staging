import FilterOption from "../FilterOption/FilterOption";

import React from "react";
import propTypes from "prop-types";
import Spinner from "../../grid/spinner/Spinner";

function Options(props) {
  const isColor = props.filterType === "colors";
  const optionsList = props.filterNames[props.filterType];
  if (!optionsList) {
    return <Spinner />;
  }
  return optionsList.map((option) => (
    <FilterOption
      {...props}
      option={option}
      isColor={isColor}
      key={option.id + option.name}
    />
  ));
}
Options.propTypes = {
  filterType: propTypes.string.isRequired,
  filterNames: propTypes.object
};

export default Options;

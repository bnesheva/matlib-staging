import React from "react";
import { shallow } from "enzyme";
import Options from "./Options";
import { default as data } from "../../../../data";

describe("Test component: <Options />", () => {
  it("Renders tags without crashing: <Options />", () => {
    const wrapper = shallow(
      <Options
        data={data}
        filterType={"tags"}
        currentFilters={[]}
        handleChange={() => {}}
        filterNames={{ tags: [{ id: 1, name: "Glossy", taggable_id: 1700 }] }}
      />
    );

    expect(wrapper.exists()).toBe(true);
  });
});

import React from "react";
import { shallow } from "enzyme";
import Pills from "./Pills";

describe("Test component: <Pills />", () => {
  it("Renders pill without crashing: <Pills />", () => {
    const wrapper = shallow(
      <Pills
        filter={"tags"}
        currentFilters={["materials_2_Leather"]}
        handleChange={() => {}}
      />
    );

    expect(wrapper.exists()).toBe(true);
  });
});

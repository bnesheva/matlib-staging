import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./Icon.css";
import PropTypes from "prop-types";

const Icon = (props) => {
  return (
    <FontAwesomeIcon
      icon={[props.iconBoldness, props.iconName]}
      className={props.className}
    />
  );
}

Icon.propTypes = {
  className: PropTypes.string,
  iconBoldness: PropTypes.string,
  iconName: PropTypes.string
};

export default Icon;

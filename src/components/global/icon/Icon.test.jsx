import React from "react";
import { shallow } from "enzyme";
import Icon from "./Icon.jsx";

describe("Test component: <Icon />", () => {
  it("Renders without crashing: <Icon />", () => {
    const wrapper = shallow(<Icon />);

    expect(wrapper.exists()).toBe(true);
  });

  it("Renders child element FontAwesomeIcon", () => {
    const wrapper = shallow(<Icon />);

    expect(wrapper.find("FontAwesomeIcon").length).toBe(1);
  });
});

import React from "react";
import { shallow } from "enzyme";
import Tooltip from "./Tooltip.jsx";

describe("Test component: <Tooltip />", () => {
  it("Renders without crashing: <Tooltip />", () => {
    const wrapper = shallow(<Tooltip />);

    expect(wrapper.exists()).toBe(true);
  });
});

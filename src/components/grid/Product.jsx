import React from "react";
import propTypes from "prop-types";
import LikeButton from "../../containers/favorites/LikeButton/LikeButton";

const Product = (props) => {
  const { user, product, manufacturer } = props;
  return (
    <div key={product.id} className="product">
      <img src={product.thumb} />
      {user ? <LikeButton productId={product.id} /> : <></>}
      <h3>{product.name}</h3>
      <h4>{manufacturer.name}</h4>
      <p>{product.fileName} </p>
    </div>
  );
};
Product.propTypes = {
  product: propTypes.object,
  manufacturer: propTypes.object,
  user: propTypes.object
};

export default Product;

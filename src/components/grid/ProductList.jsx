/* eslint-disable react/prop-types */
import React, { useEffect, useState } from "react";
import Spinner from "../grid/spinner/Spinner";
import Product from "../../containers/grid/Product";
import NoResult from "../grid/noresult/NoResult";
import { handleScroll } from "../grid/handleScroll";
import { useDispatch } from "react-redux";
import { clearProducts } from "../../actions/fetchingActions";

export default function ProductList(props) {
  const {
    currentFilters,
    fetchList,
    products,
    fetchProductManufacturers,
    manufacturers,
    productsNum,
    productIds,
    sorterType,
    searching
  } = props;

  const [pageNumber, setPageNumber] = useState(1);
  const dispatch = useDispatch();

  useEffect(() => {
    fetchProductManufacturers();
  }, []);

  useEffect(() => {
    window.addEventListener("scroll", () => handleScroll(setPageNumber));
    return () =>
      window.removeEventListener("scroll", () => handleScroll(setPageNumber));
  }, []);

  useEffect(() => {
    dispatch(clearProducts());
    setPageNumber(() => 1);
  }, [currentFilters, sorterType, searching]);

  useEffect(() => {
    if (productsNum != 0) {
      fetchList({
        page: pageNumber,
        filters: currentFilters,
        productIds: productIds ? productIds : "",
        sort: sorterType,
        searching: searching
      });
    }
  }, [pageNumber, currentFilters, sorterType, searching]);

  function renderProduct(product) {
    if (manufacturers.length === 0) return <Spinner key={product.id} />;
    if (manufacturers) {
      let manufacturer = manufacturers.find((o) => o.id === product.manufacturerId);
      return (
        <Product key={product.id} product={product} manufacturer={manufacturer} />
      );
    }
  }
  return (
    <section id="products">
      {products && products.length ? (
        products.map(renderProduct)
      ) : (
        <NoResult message="no tiles found matching your search criteria" />
      )}
    </section>
  );
}

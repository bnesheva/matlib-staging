import React, { useState } from "react";
//import propTypes from "prop-types";

function BackToTop() {
  const [ShowBackToTop, setShowBackToTop] = useState(false);

  const checkScrollTop = () => {
    if (!ShowBackToTop && window.pageYOffset > 400) {
      setShowBackToTop(true);
    } else if (ShowBackToTop && window.pageYOffset <= 400) {
      setShowBackToTop(false);
    }
  };

  const scrollTop = () => {
    window.scrollTo({ top: 0, behavior: "smooth" });
  };

  window.addEventListener("scroll", checkScrollTop);

  return (
    <>
      <div
        className="top"
        onClick={scrollTop}
        style={{ display: ShowBackToTop ? "block" : "none" }}
      >
        <i className="fas fa-chevron-up"></i>
      </div>
    </>
  );
}

//BackToTop.propTypes = {};

export default BackToTop;

import React from "react";
import { shallow } from "enzyme";
import BackToTop from "./BackToTop";

describe("Test component: <BackToTop />", () => {
  it("Renders tags without crashing: <BackToTop />", () => {
    const backToTop = shallow(<BackToTop />);

    expect(backToTop.exists()).toBe(true);
  });

  it("Should render 1 div in BackToTop", () => {
    const wrapper = shallow(<BackToTop />);

    expect(wrapper.find("div").length).toBe(1);
  });

  it("Should simulate click and verify scrollTo function has been called", () => {
    window.pageYOffset = 200;
    window.scrollTo = jest.fn();
    const wrapper = shallow(<BackToTop />);
    wrapper.find(".top").simulate("click");

    expect(window.scrollTo).toBeCalled();
  });
});

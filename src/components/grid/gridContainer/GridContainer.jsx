import React from "react";
import propTypes from "prop-types";
import "../../../App.css";
import "./GridContainer.css";
import Pills from "../../filters/Pills/Pills";
import makeFilterList from "../../../services/filter/makeFilterList";
import Search from "../../../containers/search/Search";
import Filters from "../../filters/Filters/Filters";
import ProductList from "../ProductList";
import Sorter from "../../../containers/sort/Sorter";

GridContainer.propTypes = {
  productIds: propTypes.string,
  setFilter: propTypes.func,
  currentFilters: propTypes.array,
  filterNames: propTypes.object,
  fetchList: propTypes.func,
  fetchProductManufacturers: propTypes.func,
  products: propTypes.array,
  spinner: propTypes.bool,
  manufacturers: propTypes.array,
  productsNum: propTypes.number,
  sorterType: propTypes.string,
  searching: propTypes.string
};

export default function GridContainer(props) {
  const {
    setFilter,
    currentFilters,
    fetchList,
    spinner,
    products,
    fetchProductManufacturers,
    manufacturers,
    productsNum,
    productIds,
    sorterType,
    searching
  } = props;
  console.log("grid-container");
  console.log(props);
  function filterProducts(e) {
    const filtersJustSet = makeFilterList(e, currentFilters);
    setFilter(filtersJustSet);
  }
  return (
    <>
      <div>
        <div className="bar-left">
          <Filters
            filterNames={props.filterNames}
            handleChange={(e) => {
              filterProducts(e);
            }}
            currentFilters={currentFilters}
          />
        </div>
      </div>
      <div className="panel-right">
        <div className="bar-top">
          <ul className="pill-box">
            <Pills
              currentFilters={currentFilters}
              handleChange={(e) => {
                filterProducts(e);
              }}
            />
          </ul>
          <Sorter />
          <Search />
        </div>
        <div className="grid">
          <ProductList
            productsNum={productsNum}
            currentFilters={currentFilters}
            fetchList={fetchList}
            spinner={spinner}
            products={products}
            manufacturers={manufacturers}
            fetchProductManufacturers={fetchProductManufacturers}
            productIds={productIds}
            sorterType={sorterType}
            searching={searching}
          />
        </div>
      </div>
    </>
  );
}

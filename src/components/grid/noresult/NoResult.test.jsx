import React from "react";
import { shallow } from "enzyme";
import NoResult from "./NoResult";

describe("Test component: <NoResult />", () => {
  it("Renders tags without crashing: <NoResult />", () => {
    const wrapper = shallow(<NoResult />);

    expect(wrapper.exists()).toBe(true);
  });

  it("Should render 2 div elements in NoResult", () => {
    const wrapper = shallow(<NoResult />);
    expect(wrapper.find("div").length).toBe(2);
  });
});

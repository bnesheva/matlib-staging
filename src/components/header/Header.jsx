import React from "react";
import { Favorites } from "./headerElements/Favorites";
import { Profile } from "./headerElements/Profile";
import { Logo } from "./headerElements/Logo";
import { Login } from "./headerElements/Login";
import PropTypes from "prop-types";

export const Header = (props) => {
  const { user, headerPin, togglePin, favList } = props;
  console.log(favList);
  return (
    <div className="header">
      <Logo />
      <div className="header-panel-right">
        <div className={headerPin ? "pin" : "pin rotate45"} onClick={togglePin}>
          <i className="fas fa-thumbtack"></i>
          Pin header
        </div>

        {user ? (
          <>
            <Profile user={user} />
            <Favorites favoritesList={favList} />
          </>
        ) : (
          <Login />
        )}
      </div>
    </div>
  );
};

Header.propTypes = {
  user: PropTypes.object,
  headerPin: PropTypes.bool.isRequired,
  togglePin: PropTypes.func.isRequired,
  favList: PropTypes.array
};

import React from "react";
import { Link } from "react-router-dom";

export const Login = () => {
  return (
    <div className="login-link">
      <Link to="/login">Login</Link>
    </div>
  );
}

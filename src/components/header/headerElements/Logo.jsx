import React from "react";
import { Link, Route } from "react-router-dom";

export const Logo = () => {
  return (
    <>
      <div className="home-link">
        <Link to="/">MatLib</Link>
      </div>
      <Route
        path="/favorites"
        component={() => <h2> &nbsp; | &nbsp;Favorites</h2>}
      ></Route>
    </>
  );
};

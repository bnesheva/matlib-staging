import React from "react";
import { shallow } from "enzyme";
import LoginScreen from "./LoginScreen";

describe("Test component: <LoginScreen />", () => {
  it("Renders tags without crashing: <LoginScreen />", () => {
    const loginScreen = shallow(<LoginScreen logIn={() => {}} />);

    expect(loginScreen.exists()).toBe(true);
  });
});

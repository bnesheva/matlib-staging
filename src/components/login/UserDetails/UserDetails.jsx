import "./user-details.css";
import React, { useState } from "react";
import propTypes from "prop-types";
function UserDetails(props) {
  const [editMode, setEditMode] = useState(false);
  const { user, editUserDetail, logOut } = props;
  const [details, setDetails] = useState(user);
  if (details.password) {
    delete details.password;
  }
  function setInputVal(event) {
    const result = event.target;
    const name = result.name;
    setDetails((details) => ({
      ...details,
      [name]: result.value
    }));
  }
  function startEdit() {
    setEditMode(true);
  }
  function finishEdit() {
    setEditMode(false);
    setDetails(user);
  }
  function cancelEdit() {
    document.getElementById("form-details").reset();
    finishEdit();
  }
  function handleSubmit(event) {
    event.preventDefault();
    editUserDetail(details);
    finishEdit();
  }
  function signOut() {
    logOut(null);
    localStorage.setItem("accessToken", null);
  }
  return (
    <div className="user-details">
      <h2>
        Welcome, {user.firstName ? user.firstName : user.email}!
        <button onClick={signOut}>Sign Out</button>
      </h2>
      <h1>
        Account Details {editMode ? null : <button onClick={startEdit}>Edit</button>}
      </h1>
      <form id="form-details" onSubmit={handleSubmit}>
        <div className="user-details__columns">
          <div className="user-details__columns__left">
            <label htmlFor="first-name">First Name</label>
            <input
              type="text"
              id="first-name"
              name="firstName"
              defaultValue={user.firstName ? user.firstName : null}
              placeholder={user.firstName ? null : "please enter your first name"}
              disabled={editMode ? null : "disabled"}
              onChange={(event) => setInputVal(event)}
            />
            <label htmlFor="first-name">Last Name</label>
            <input
              type="text"
              id="last-name"
              name="lastName"
              defaultValue={user.lastName ? user.lastName : null}
              placeholder={user.lastName ? null : "please enter your last name"}
              disabled={editMode ? null : "disabled"}
              onChange={(event) => setInputVal(event)}
            />
            <label htmlFor="email">Email</label>
            <input
              type="email"
              id="email"
              defaultValue={user.email}
              disabled="disabled"
            />
          </div>
          <div className="user-details__columns__right">
            <img
              src={
                details.photoUrl
                  ? details.photoUrl
                  : "https://www.jbrhomes.com/wp-content/uploads/blank-avatar.png"
              }
              alt="user avatar"
            />
            {editMode ? (
              <>
                <label htmlFor="avatar">Profile Picture URL</label>
                <input
                  type="text"
                  id="avatar"
                  name="photoUrl"
                  defaultValue={user.photoUrl ? user.photoUrl : null}
                  placeholder={user.photoUrl ? null : "please enter image URL"}
                  onChange={(event) => setInputVal(event)}
                />
              </>
            ) : null}
          </div>
        </div>
        {editMode ? (
          <div className="user-details__bottom">
            <input
              type="reset"
              className="user-details__cancel"
              value="Cancel"
              onClick={cancelEdit}
            ></input>
            <input
              type="submit"
              className="user-details__save"
              value="Save Changes"
            ></input>
          </div>
        ) : null}
      </form>
    </div>
  );
}

UserDetails.propTypes = {
  logOut: propTypes.func.isRequired,
  user: propTypes.object,
  editUserDetail: propTypes.func.isRequired
};

export default UserDetails;

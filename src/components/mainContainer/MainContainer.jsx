import React from "react";
import { routing } from "../../services/routing/routing";
import propTypes from "prop-types";

export const MainContainer = (props) => {
  const { user } = props;
  return <div className="container-main">{routing(user)}</div>;
};

MainContainer.propTypes = {
  user: propTypes.object
};

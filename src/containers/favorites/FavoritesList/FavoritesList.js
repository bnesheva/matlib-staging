import { connect } from "react-redux";
import { FavoritesList } from "../../../components/favorites/FavoritesList/FavoritesList";
import { fetchFavorites } from "../../../services/useFetchProducts";

const mapStateToProps = (state) => {
  const { favList } = state;
  return { favList };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchFavoritesList: () => dispatch(fetchFavorites(dispatch))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FavoritesList);

import { connect } from "react-redux";
import { FavoritesNumber } from "../../../components/favorites/FavoritesNumber/FavoritesNumber";

const mapStateToProps = (state) => {
  const { favList } = state;
  return { favList };
};

export default connect(mapStateToProps)(FavoritesNumber);

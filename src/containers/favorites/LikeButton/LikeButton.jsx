import { connect } from "react-redux";
import LikeButtonWithTooltip from "../../../services/grid/product/HeartButtonOnProduct/LikeButtonWithTooltip";
import { addFavorites, removeFavorites } from "../../../services/useFetchProducts";

const mapStateToProps = (state, ownProps) => {
  const { favList } = state;
  const { productId } = ownProps;
  return { favList, productId };
};

const mapDispatchToProps = (dispatch) => {
  return {
    addFavorite: (productId) => dispatch(addFavorites(productId)),
    removeFavorite: (favoriteId) => dispatch(removeFavorites(favoriteId))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(LikeButtonWithTooltip);

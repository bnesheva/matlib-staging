import { connect } from "react-redux";
import { fetchProducts, fetchManufacturer } from "../../services/useFetchProducts";
import GridContainer from "../../components/grid/gridContainer/GridContainer";

const mapStateToProps = (state) => {
  const { fetchProducts, manufacturers } = state;
  const spinner = fetchProducts.spinner;
  const products = fetchProducts.products;
  const productsNum = fetchProducts.productsNum;
  return { spinner, products, manufacturers, productsNum };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchList: (params) => dispatch(fetchProducts(params, dispatch)),
    fetchProductManufacturers: () => dispatch(fetchManufacturer(dispatch))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(GridContainer);

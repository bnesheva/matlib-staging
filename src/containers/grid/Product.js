import { connect } from "react-redux";
import Product from "../../components/grid/Product";

const mapStateToProps = (state, ownProps) => {
  const { user } = state;
  const { key } = ownProps;
  const { product } = ownProps;
  const { manufacturer } = ownProps;

  return { user, key, product, manufacturer };
};

export default connect(mapStateToProps)(Product);

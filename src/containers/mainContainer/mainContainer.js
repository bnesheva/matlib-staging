import { connect } from "react-redux";
import { MainContainer } from "../../components/mainContainer/MainContainer";

const mapStateToProps = (state) => {
  const { user } = state;
  return { user };
};

export default connect(mapStateToProps)(MainContainer);

import { connect } from "react-redux";
import { search } from "../../actions/searchAction";
import SearchBox from "../../components/grid/SearchBox";

function mapStateToProps(state) {
  const { searching } = state;
  return { searching };
}

const mapDispatchToProps = (dispatch) => {
  return {
    search: (searching) => dispatch(search(searching))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBox);

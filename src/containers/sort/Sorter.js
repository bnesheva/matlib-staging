import { connect } from "react-redux";
import { sort } from "../../actions/sortActions";
import Sorter from "../../components/grid/Sorter/Sorter";

function mapStateToProps(state) {
  const { sorterType } = state;
  return { sorterType };
}

const mapDispatchToProps = (dispatch) => {
  return {
    sort: (sorterType) => dispatch(sort(sorterType))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Sorter);

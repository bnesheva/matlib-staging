import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { AppContainer } from "react-hot-loader";
import { Provider } from "react-redux";
// import { store } from "../src/state/store";
import { createStore, applyMiddleware, compose } from "redux";
import rootReducer from "./reducers/rootReducer";
import { devToolsEnhancer } from "redux-devtools-extension";
import thunk from "redux-thunk";

require("react-hot-loader/patch");

const enhancers = [applyMiddleware(thunk), devToolsEnhancer()];

export const store = createStore(rootReducer, compose(...enhancers));

ReactDOM.render(
  <Provider store={store}>
    <AppContainer>
      <App />
    </AppContainer>
  </Provider>,
  document.getElementById("root")
);

if (module.hot) {
  module.hot.accept("./App", () => {
    const NextApp = require("./App").default;

    ReactDOM.render(
      <AppContainer>
        <NextApp />
      </AppContainer>,
      document.getElementById("root")
    );
  });
}

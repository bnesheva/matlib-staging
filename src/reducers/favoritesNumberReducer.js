import { ADD_FAV, REMOVE_FAV, FETCH_FAVORITES_SUCCESS } from "../actions/constants";

export const favoritesListReducer = (state = [], action) => {
  switch (action.type) {
    case ADD_FAV:
      return [...state, action.productId];
    case REMOVE_FAV:
      return state.filter((favorite) => favorite.id !== action.productId);
    case FETCH_FAVORITES_SUCCESS:
      return action.favorites;
    default:
      return state;
  }
};

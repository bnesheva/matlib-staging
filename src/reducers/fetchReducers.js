import {
  SPINNER,
  FETCH_SUCCESS,
  FETCH_SUCCESS_MANUFACTURER,
  CLEAR_PRODUCTS
} from "../actions/constants";

const initialState = {
  spinner: false,
  products: [],
  productsNum: null,
  error: null
};

export const productsFetchReducer = (state = initialState, action) => {
  switch (action.type) {
    case SPINNER:
      return {
        ...state,
        spinner: true
      };
    case FETCH_SUCCESS:
      return {
        ...state,
        spinner: false,
        productsNum: action.products.length,
        products: [...state.products, ...action.products]
      };
    case CLEAR_PRODUCTS:
      return {
        ...state,
        spinner: false,
        products: []
      };
    default:
      return state;
  }
};

export const manufacturersFetchReducer = (state = [], action) => {
  switch (action.type) {
    case FETCH_SUCCESS_MANUFACTURER:
      return action.manufacturers;
    default:
      return state;
  }
};

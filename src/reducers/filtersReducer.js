import { MAKE_LIST } from "../actions/constants";

export const filtersReducer = (state = [], action) => {
  switch (action.type) {
    case MAKE_LIST:
      return action.currentFilters;
    default:
      return state;
  }
};

import { filtersReducer } from "./filtersReducer";
import { filter } from "../actions/filterActions";

describe("make a list of filters", () => {
  it("return a list of filters", () => {
    const next = filtersReducer(
      [],
      filter({ type: "MAKE_LIST", currentFilters: [{}] })
    );
    expect(next).toMatchSnapshot();
  });
  it("return default", () => {
    const next = filtersReducer([], filter({ type: "NO_TYPE" }));
    expect(next).toMatchSnapshot();
  });
});

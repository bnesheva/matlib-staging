import { combineReducers } from "redux";
import { favoritesListReducer } from "./favoritesNumberReducer";
import { logInReducer } from "./authReducers";
import { filtersReducer } from "./filtersReducer";
import { productsFetchReducer, manufacturersFetchReducer } from "./fetchReducers";
import { userDetailsReducer } from "./userDetailsReducer";
import { sortReducers } from "./sortReducers";
import { searchingReducer } from "./searchReducer";

const rootReducer = combineReducers({
  user: logInReducer,
  favList: favoritesListReducer,
  currentFilters: filtersReducer,
  fetchProducts: productsFetchReducer,
  manufacturers: manufacturersFetchReducer,
  details: userDetailsReducer,
  sorterType: sortReducers,
  searching: searchingReducer
});

export default rootReducer;

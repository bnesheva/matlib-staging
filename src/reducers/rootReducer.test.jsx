import { createStore } from "redux";
import rootReducer from "./rootReducer";
import { logInReducer } from "./authReducers";
import { favoritesListReducer } from "./favoritesNumberReducer";
import { filtersReducer } from "./filtersReducer";
import { productsFetchReducer, manufacturersFetchReducer } from "./fetchReducers";
import { userDetailsReducer } from "./userDetailsReducer";


const initialState = {
  spinner: false,
  products: [],
  productsNum: null,
  error: null
};

const store = createStore(rootReducer);

it("check that initial state of the root reducer matches", () => {
  expect(store.getState().user).toEqual(logInReducer(null, {}));
  expect(store.getState().favList).toEqual(favoritesListReducer([], {}));
  expect(store.getState().currentFilters).toEqual(filtersReducer([], {}));
  expect(store.getState().fetchProducts).toEqual(
    productsFetchReducer(initialState, {})
  );
  expect(store.getState().manufacturers).toEqual(manufacturersFetchReducer([], {}));
  expect(store.getState().details).toEqual(userDetailsReducer(null, {}));
});

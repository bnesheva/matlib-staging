import { EDIT_DETAILS } from "../actions/constants";

export const userDetailsReducer = (state = null, action) => {
  switch (action.type) {
    case EDIT_DETAILS:
      return action.details;
    default:
      return state;
  }
};

import { default as data } from "../../../data";

export const getListOfUserFavorites = (favorites) => {
  return new Set(
    favorites
      .filter((favorite) => favorite.userId == data.users[0].id)
      .map((x) => x.vrscanId)
  );
};

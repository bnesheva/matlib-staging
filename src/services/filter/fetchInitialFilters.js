import React from "react";
import useFetch from "../useFetch";
import Spinner from "../../components/grid/spinner/Spinner";
import { values } from "../../constants";

export default function fetchInitialFilters() {
  function combineFilterNames() {
    let filterNames = {};
    values.FilterTypes.map((type) => {
      let filtersNames = getFilterNames(type);
      if (filtersNames.length) {
        filterNames[type] = filtersNames;
      }
    });
    return filterNames;
  }
  function getFilterNames(type) {
    let namesFetched = useFetch(type, false);
    if (namesFetched.error) throw namesFetched.error;
    if (namesFetched.loading) {
      return <Spinner />;
    }
    if (namesFetched.data) {
      return namesFetched.data;
    }
  }
  var filterNames;
  const fetchedFilters = combineFilterNames();
  if (Object.keys(fetchedFilters).length === values.FilterTypes.length) {
    filterNames = fetchedFilters;
    return filterNames;
  } else {
    return <Spinner />;
  }
}

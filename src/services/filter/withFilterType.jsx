import React from "react";
import propTypes from "prop-types";

function withFilterType(WrappedComponent, filter, props) {
  return <WrappedComponent filterType={filter} {...props} key={filter} />;
}

withFilterType.propTypes = {
  filter: propTypes.string.isRequired
};

export default withFilterType;

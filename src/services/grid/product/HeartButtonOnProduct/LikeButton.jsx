import React from "react";
import Icon from "../../../../components/global/icon/Icon";
import PropTypes from "prop-types";
import { values } from "../../../../constants";

const className = values.LikeButton.className;
const icon = values.LikeButton.iconName;
function LikeButton(props) {
  const setIconBoldness = props.isLiked
    ? values.LikeButton.solidStyle
    : values.LikeButton.regularStyle;
  return (
    <Icon className={className} iconName={icon} iconBoldness={setIconBoldness} />
  );
}

LikeButton.propTypes = {
  isLiked: PropTypes.bool
};

export default LikeButton;

import React, { useState } from "react";
import LikeButton from "./LikeButton";
import TooltipOnProductLike from "./ToolTipOnProductLike";
import { values } from "../../../../constants";
import PropTypes from "prop-types";

export default function LikeButtonWithTooltip(props) {
  const { addFavorite, removeFavorite, favList, productId } = props;
  const isProductLiked = favList
    .map((favorite) => favorite.vrscanId)
    .includes(productId);

  const [isLiked, setLiked] = useState(isProductLiked);
  const [isMouseOver, setIsMouseOver] = useState(false);
  const likeButton = <LikeButton isLiked={isLiked} />;
  const tooltip = <TooltipOnProductLike isLiked={isLiked} />;
  const hover = (
    <span className={values.LikeButtonWithTooltip.className}>
      {tooltip} {likeButton}
    </span>
  );

  const setIsLiked = () => {
    setLiked(!isLiked);
    setProductFavorite(isLiked);
  };

  const getFavoriteId = (favList, productId) => {
    const result = favList
      .filter((favorite) => favorite.vrscanId == productId)
      .map((favorite) => favorite.id);
    return result[0];
  };

  const setProductFavorite = (isLiked) => {
    isLiked
      ? removeFavorite(getFavoriteId(favList, productId))
      : addFavorite(productId);
  };

  return (
    <span
      onClick={setIsLiked}
      onMouseOver={() => {
        setIsMouseOver(true);
      }}
      onMouseOut={() => {
        setIsMouseOver(false);
      }}
    >
      {isMouseOver ? hover : likeButton}
    </span>
  );
}

LikeButtonWithTooltip.propTypes = {
  addFavorite: PropTypes.func,
  removeFavorite: PropTypes.func,
  favList: PropTypes.array,
  productId: PropTypes.number
};

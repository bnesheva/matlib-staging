import React from "react";
import Tooltip from "../../../../components/global/tooltip/Tooltip";
import PropTypes from "prop-types";
import { values } from "../../../../constants";

const className = values.TooltipOnProductLike.className;
function TooltipOnProductLike(props) {
  const setText = props.isLiked ? (
    <span>{values.TooltipOnProductLike.removeMe}</span>
  ) : (
    <span>{values.TooltipOnProductLike.addMe}</span>
  );

  return <Tooltip content={setText} className={className} />;
}

TooltipOnProductLike.propTypes = {
  isLiked: PropTypes.bool
};

export default TooltipOnProductLike;

import React from "react";
import { HashRouter as Router, Switch, Route, Redirect } from "react-router-dom";
import Filter from "../../containers/filter/Filter";
import Favorites from "../../containers/favorites/FavoritesList/FavoritesList";
import LoginScreen from "../../containers/auth/LoginScreen";
import UserDetails from "../../containers/userDetails/UserDetails";
import propTypes from "prop-types";
import fetchInitialFilters from "../filter/fetchInitialFilters";

export const routing = (user) => {
  return (
    <Router>
      <Switch>
        <Route exact path="/login" component={() => <LoginScreen />}></Route>
        <Route
          exact
          path="/"
          component={() => <Filter filterNames={fetchInitialFilters()} />}
        ></Route>
        <Route
          path="/favorites"
          component={() => (user ? <Favorites /> : <Redirect to="/" />)}
        ></Route>
        <Route
          path="/profile"
          component={() =>
            user ? <UserDetails user={user} /> : <Redirect to="/" />
          }
        ></Route>
      </Switch>
    </Router>
  );
};

routing.propTypes = {
  user: propTypes.object
};
